<?php

// Start session
session_start();

// Retrieve session data
$sessData = !empty($_SESSION['sessData']) ? $_SESSION['sessData'] : '';

// Include and initialize JSON class
require_once 'library/Json.class.php';
$db = new Json();

// Fetch the member's data
$members = $db->getTransactionRows();

// Get status message from session
if (!empty($sessData['status']['msg'])) {
    $statusMsg = $sessData['status']['msg'];
    $statusMsgType = $sessData['status']['type'];
    unset($_SESSION['sessData']['status']);
}
?>

<!-- Display status message -->
<?php if (!empty($statusMsg) && ($statusMsgType == 'success')) { ?>
    <div class="col-xs-12">
        <div class="alert alert-success"><?php echo $statusMsg; ?></div>
    </div>
<?php } elseif (!empty($statusMsg) && ($statusMsgType == 'error')) { ?>
    <div class="col-xs-12">
        <div class="alert alert-danger"><?php echo $statusMsg; ?></div>
    </div>
<?php } ?>
<!DOCTYPE html>
<html>
<head>
    <title>Transaction Information</title>
    <?php require 'assets/autoloader.php'; ?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 head">
            <h2 style="text-align: center;padding-top: 10px">Transaction Information</h2>
            <!-- Add link -->
            <div class="float-right" style="padding: 10px">
                <a href="index.php" class="btn btn-success"><i class="plus"></i> Account Information</a>
                <a href="transferAmount.php" class="btn btn-warning">Transfer Amount</a>
            </div>
        </div>

        <!-- List the users -->
        <table id="example" class="display cell-border">
            <thead>
            <tr>
                <th>#</th>
                <th style="text-align: left">Transaction Action</th>
                <th style="text-align: left">Credit</th>
                <th style="text-align: left">Debit</th>
                <th style="text-align: left">Account Number</th>
                <th style="text-align: left">Account ID</th>
                <th style="text-align: left">Comment</th>
                <th style="text-align: left">Transaction Date</th>
                <!--<th style="text-align: left">Action</th>-->
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($members)) {
                $count = 0;
                foreach ($members as $row) {
                    $count++; ?>
                    <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $row['action']; ?></td>
                        <td><?php echo $row['credit']; ?></td>
                        <td><?php echo $row['debit']; ?></td>
                        <td><?php echo $row['other']; ?></td>
                        <td><?php echo $row['userId']; ?></td>
                        <td><?php echo $row['comment']; ?></td>
                        <td><?php echo date("Y/m/d", strtotime($row['date'])); ?></td>

                    </tr>
                <?php }
            } else { ?>
                <tr>
                    <td colspan="6">No member(s) found...</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>