<?php

// Start session
session_start();

// Retrieve session data
$sessData = !empty($_SESSION['sessData']) ? $_SESSION['sessData'] : '';

// Include and initialize JSON class
require_once 'library/Json.class.php';
$db = new Json();

// Fetch the member's data
$members = $db->getAccountRows();

// Get status message from session
if (!empty($sessData['status']['msg'])) {
    $statusMsg = $sessData['status']['msg'];
    $statusMsgType = $sessData['status']['type'];
    unset($_SESSION['sessData']['status']);
}
?>

<!-- Display status message -->
<?php if (!empty($statusMsg) && ($statusMsgType == 'success')) { ?>
    <div class="col-xs-12">
        <div class="alert alert-success"><?php echo $statusMsg; ?></div>
    </div>
<?php } elseif (!empty($statusMsg) && ($statusMsgType == 'error')) { ?>
    <div class="col-xs-12">
        <div class="alert alert-danger"><?php echo $statusMsg; ?></div>
    </div>
<?php } ?>
<!DOCTYPE html>
<html>
<head>
    <title>Account Information</title>
    <?php require 'assets/autoloader.php'; ?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 head">
            <h2 style="text-align: center;padding-top: 10px">Account Information</h2>
            <!-- Add link -->
            <div class="float-right" style="padding: 10px">
                <!--<a href="addEditAccount.php" class="btn btn-success"><i class="plus"></i> Add Account Information</a>-->
                <a href="transaction.php" class="btn btn-success"><i class="plus"></i> Transaction Information</a>
            </div>
        </div>

        <!-- List the users -->
        <table id="example" class="display cell-border">
            <thead>
            <tr>
                <th>#</th>
                <th style="text-align: left">Email</th>
                <th style="text-align: left">Name</th>
                <th style="text-align: left">Balance</th>
                <th style="text-align: left">Account Number</th>
                <th style="text-align: left">Phone Number</th>
                <th style="text-align: left">city</th>
                <th style="text-align: left">address</th>
                <th style="text-align: left">branch</th>
                <th style="text-align: left">accountType</th>
                <th style="text-align: left">date</th>
                <!--<th style="text-align: left">Action</th>-->
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($members)) {
                $count = 0;
                foreach ($members as $row) {
                    $count++; ?>
                    <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['balance']; ?></td>
                        <td><?php echo $row['accountNo']; ?></td>
                        <td><?php echo $row['number']; ?></td>
                        <td><?php echo $row['city']; ?></td>
                        <td><?php echo $row['address']; ?></td>
                        <td><?php echo $row['branch']; ?></td>
                        <td><?php echo $row['accountType']; ?></td>
                        <td><?php echo date("Y/m/d", strtotime($row['date'])); ?></td>
                        <!--<td>
                        <a href="addEditAccount.php?id=<?php /*echo $row['id']; */ ?>" class="btn btn-warning">edit</a>
                        <a href="userAction.php?action_type=delete&id=<?php /*echo $row['id']; */ ?>" class="btn btn-danger"
                           onclick="return confirm('Are you sure to delete?');">delete</a>
                    </td>-->
                    </tr>
                <?php }
            } else { ?>
                <tr>
                    <td colspan="6">No member(s) found...</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>