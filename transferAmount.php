<?php
// Start session
session_start();

// Retrieve session data
$sessData = !empty($_SESSION['sessData']) ? $_SESSION['sessData'] : '';

// Get member data


// Include and initialize JSON class
include 'library/Json.class.php';
$db = new Json();
$accounts = $db->getAccountRows();
// Fetch the member data


$actionLabel = !empty($_GET['id']) ? 'Edit' : 'Add';

// Get status message from session
if (!empty($sessData['status']['msg'])) {
    $statusMsg = $sessData['status']['msg'];
    $statusMsgType = $sessData['status']['type'];
    unset($_SESSION['sessData']['status']);
}
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<!-- Display status message -->
<?php if (!empty($statusMsg) && ($statusMsgType == 'success')) { ?>
    <div class="col-xs-12">
        <div class="alert alert-success"><?php echo $statusMsg; ?></div>
    </div>
<?php } elseif (!empty($statusMsg) && ($statusMsgType == 'error')) { ?>
    <div class="col-xs-12">
        <div class="alert alert-danger"><?php echo $statusMsg; ?></div>
    </div>
<?php } ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 style="text-align: center;padding-top: 10px">Transfer Amount</h2>
        </div>
        <div class="col-md-6">
            <form method="post" action="userAction.php">
                <div class="form-group">
                    <label>From Account</label>
                    <select class="form-control" name="from_account">
                        <option value="">Select From Account</option>
                        <?php
                        foreach ($accounts as $value) {
                            ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>

                            <?php
                        }
                        ?>

                    </select>
                </div>
                <div class="form-group">
                    <label>To Account</label>

                    <select class="form-control" name="to_account">
                        <option value="">Select To Account</option>
                        <?php
                        foreach ($accounts as $value) {
                            ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>

                            <?php
                        }
                        ?>

                    </select>
                </div>
                <div class="form-group">
                    <label>amount</label>
                    <input type="text" class="form-control" name="amount" placeholder="Enter your amount" value=""
                           required="">
                </div>
                <div class="form-group">
                    <label>comment</label>
                    <input type="text" class="form-control" name="comment" placeholder="Enter your comment" value=""
                           required="">
                </div>
                <a href="index.php" class="btn btn-secondary">Back</a>

                <input type="submit" name="transferAmount" class="btn btn-success" value="Submit">
            </form>
        </div>
    </div>
</div>